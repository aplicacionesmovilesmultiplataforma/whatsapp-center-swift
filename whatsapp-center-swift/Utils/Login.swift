
import Foundation
import UIKit
struct dataUser:Codable {
    var usuario:String?
    var clave:String?
}
struct Usuario:Decodable {
    let idusuario: Int?
    let idempresa: Int?
    let razonsocial :String?
    let apellidos:String?
    let nombres: String?
    let idrol: Int?
    let rol: String?
    let usuario :String?
    let clave: String?
}
class Login{
    var usuarios = [Usuario]()
    func HttpPostLogin(UrlHttp:String, data:dataUser
        ,completed:@escaping () -> ()){
        let url = URL(string: UrlHttp )
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl )
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONEncoder().encode(data)
        let http = URLSession.shared.dataTask(with: request ) {( data, response,error) in
            if error != nil {
                print("Error en la peticion")
                return
            }
            guard let data = data else { return }
            do{
                let dataResponse = try JSONDecoder().decode(Usuario.self,from: data)
                self.usuarios.append(dataResponse)
                DispatchQueue.main.async{
                    completed()
                }
            }catch let jsonError{
                print(jsonError)
            }
        }
        http.resume()
    }
    func UserPreferences (idusuario:Int,idempresa: Int,nombres: String,apellidos:String,idrol: Int,rol: String,usuario :String){
        UserDefaults.standard.set(idusuario, forKey: "UserIdUsuario")
        UserDefaults.standard.set(idempresa, forKey: "UserIdEmpresa")
        UserDefaults.standard.set(nombres, forKey: "UserNombre")
        UserDefaults.standard.set(apellidos, forKey: "UserApellido")
        UserDefaults.standard.set(idrol, forKey: "UserIdRol")
        UserDefaults.standard.set(rol, forKey: "UserRol")
        UserDefaults.standard.set(usuario, forKey: "UserUsuario")
        UserDefaults.standard.synchronize()

    }
    func RemoveUserPreferences (){
        UserDefaults.standard.removeObject(forKey: "UserIdUsuario")
        UserDefaults.standard.removeObject(forKey: "UserIdEmpresa")
        UserDefaults.standard.removeObject(forKey: "UserNombre")
        UserDefaults.standard.removeObject(forKey: "UserApellido")
        UserDefaults.standard.removeObject(forKey: "UserIdRol")
        UserDefaults.standard.removeObject(forKey: "UserRol")
        UserDefaults.standard.removeObject(forKey: "UserUsuario")
        UserDefaults.standard.synchronize()
    }
}
