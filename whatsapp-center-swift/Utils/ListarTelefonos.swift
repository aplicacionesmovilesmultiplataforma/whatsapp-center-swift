import Foundation
import UIKit

struct Telefono:Decodable {
    let idnumero:Int?
    let idempresa:Int?
    let razonsocial:String?
    let numero:String?
    let instance:String?
    let token:String?
}
class Telefonos{
    var telefonos = [Telefono]()
    
    func HttpGetTelefonos(idEmpresa:Int?,completed:@escaping () -> ()){
        let ruta = "https://whatsappcenter.tk:8443/api/numero/lista/\(idEmpresa!)"
        cargarTelefonos(ruta: ruta){
            completed()
        }
    }
        
    func cargarTelefonos(ruta: String, completed: @escaping () -> ()){
        let url = URL(string: ruta)
        URLSession.shared.dataTask(with: url!){ (data, response, error) in
            if error == nil{
                do{
                    self.telefonos = try JSONDecoder().decode([Telefono].self, from: data!)
                    DispatchQueue.main.async {
                        completed()
                    }
                }catch{
                    print("Error en JSON")
                }
            }
        }.resume()
    }
}
