import Foundation
import UIKit

struct Contacto:Decodable {
    let idcontacto:Int?
    let nombre:String?
    let imagen:String?
    let codigo:String?
    let fconexion:String?
    let idusuario:Int?
    let nombreusuario:String?
}
class Contactos{
    var contactos = [Contacto]()
    
    func HttpGetContactos(idNumero:Int?,idUsuario:Int?,completed:@escaping () -> ()){
        let ruta = "https://whatsappcenter.tk:8443/api/contacto/lista/\(idNumero!)/\(idUsuario!)"
        print(ruta)
        cargarContactos(ruta: ruta){
            completed()
        }
    }
        
    func cargarContactos(ruta: String, completed: @escaping () -> ()){
        let url = URL(string: ruta)
        URLSession.shared.dataTask(with: url!){ (data, response, error) in
            if error == nil{
                do{
                    self.contactos = try JSONDecoder().decode([Contacto].self, from: data!)
                    DispatchQueue.main.async {
                        completed()
                    }
                }catch{
                    print("Error en JSON")
                }
            }
        }.resume()
    }
}
