//
//  CustomCell.swift
//  whatsapp-center-swift
//
//  Created by Mac 17 on 6/27/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var photoContactImageView: UIImageView!
    @IBOutlet weak var nameContact: UILabel!
    @IBOutlet weak var usernameContact: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
