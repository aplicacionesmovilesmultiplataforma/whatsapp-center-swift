import Foundation

struct dataActualizarAgente:Codable {
    var idusuario:Int?
    var idcontacto:Int?
}

class ActualizarAgente{
    func HttpPutAgente(UrlHttp:String, data:dataActualizarAgente
        ,completed:@escaping () -> ()){
        let url = URL(string: UrlHttp )
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl )
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONEncoder().encode(data)
        let http = URLSession.shared.dataTask(with: request ) {( data, response,error) in
            if error != nil {
                print("Error en la peticion")
                return
            }else{
               DispatchQueue.main.async{
                    completed()
                }
            }
        }
        http.resume()
    }
}
