//
//  Alertas.swift
//  whatsapp-center-swift
//
//  Created by Mac 03 on 6/24/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//
import UIKit
import Foundation

class Alertas{
    func alertaSimple(_ viewController:UIViewController,title:String,message:String,action:String){
        let alertaError = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let btnOK = UIAlertAction(title: action, style: .default,handler: nil)
        alertaError.addAction(btnOK)
        viewController.present(alertaError, animated: true, completion: nil)
    }
    func alertaConAccion(_ viewController:UIViewController,title:String,message:String,action:String,handler:((UIAlertAction)->Void)?){
        let alertaError = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let btnOK = UIAlertAction(title: action, style: .default,handler: handler)
        alertaError.addAction(btnOK)
        viewController.present(alertaError, animated: true, completion: nil)
    }
    //Crear más funciones
}
