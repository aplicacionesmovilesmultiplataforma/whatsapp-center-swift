import Foundation

class Usuarios{
    var usuarios = [Usuario]()
    
    func HttpGetUsuarios(idEmpresa:Int?,completed:@escaping () -> ()){
        let ruta = "https://whatsappcenter.tk:8443/api/usuario/lista/\(idEmpresa!)"
        cargarUsuarios(ruta: ruta){
            completed()
        }
    }
        
    func cargarUsuarios(ruta: String, completed: @escaping () -> ()){
        let url = URL(string: ruta)
        URLSession.shared.dataTask(with: url!){ (data, response, error) in
            if error == nil{
                do{
                    self.usuarios = try JSONDecoder().decode([Usuario].self, from: data!)
                    DispatchQueue.main.async {
                        completed()
                    }
                }catch{
                    print("Error en JSON")
                }
            }
        }.resume()
    }
}
