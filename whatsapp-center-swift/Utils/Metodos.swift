//
//  Metodos.swift
//  whatsapp-center-swift
//
//  Created by Mac 03 on 6/27/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import Foundation
class Metodo{
    func metodo(ruta:String, datos : [String:Any],method:String){
        let url:URL  = URL(string: ruta)!
        var request = URLRequest(url:url)
        let session = URLSession.shared
        request.httpMethod = method
        let params = datos
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        }catch{
            
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request,completionHandler:
        {(data,response,error) in
            if data != nil{
                do{
                    let dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves)
                    print(dict)
                }catch{
                    
                }
            }
        })
        task.resume()
    }
    
}
