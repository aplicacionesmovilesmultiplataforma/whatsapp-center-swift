import UIKit
import SDWebImage

class AsignarContactoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var telefono:Telefono? = nil
    var contactos = [Contacto]()
    @IBOutlet weak var tableContactos: UITableView!
    @IBOutlet weak var btnNumero: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableContactos.delegate = self
        self.tableContactos.dataSource = self
        if (telefono != nil){
            btnNumero.setTitle(telefono?.numero, for: .normal)
        }
        self.tableContactos.separatorStyle = .none
    }
    
    @IBAction func tapNumero(_ sender: Any) {
        performSegue(withIdentifier: "segueSeleccionarNumero", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomCell
        let contacto = contactos[indexPath.row]
        let imageplaceholder = UIImage(named: "logo_blue")

        cell.nameContact.text = contacto.nombre!
        cell.usernameContact.text = contacto.nombreusuario!
        cell.photoContactImageView.sd_setImage(with: URL(string: contacto.imagen!), placeholderImage: imageplaceholder)
        cell.photoContactImageView.layer.masksToBounds = true
        cell.photoContactImageView.layer.cornerRadius = 20.0
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueCambiarUsuario", sender: contactos[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "segueSeleccionarNumero"){
            let siguienteVC = segue.destination as! TelefonosViewController
            siguienteVC.anteriorVC = self
        }else if (segue.identifier == "segueCambiarUsuario"){
            let siguienteVC = segue.destination as! CambiarAgenteViewController
            siguienteVC.contacto = sender as? Contacto
            siguienteVC.anteriorVC = self
        }
    }
}
