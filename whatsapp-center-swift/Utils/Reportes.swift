//
//  Reportes.swift
//  whatsapp-center-swift
//
//  Created by Mac 05 on 6/27/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import Foundation

struct Reporte:Decodable {
    let idusuario:Int?
    let apellidos:String?
    let nombres:String?
    let usuario:String?
    let contactos:Double?
}
class Reportes{
    var reportes = [Reporte]()
    
    func HttpGetReportes(idEmpresa:Int?,idUsuario:Int?,completed:@escaping () -> ()){
        let ruta = "https://whatsappcenter.tk:8443/api/reporte/\(idEmpresa!)/\(idUsuario!)"
        print(ruta)
        cargarReportes(ruta: ruta){
            completed()
        }
    }
        
    func cargarReportes(ruta: String, completed: @escaping () -> ()){
        let url = URL(string: ruta)
        URLSession.shared.dataTask(with: url!){ (data, response, error) in
            if error == nil{
                do{
                    self.reportes = try JSONDecoder().decode([Reporte].self, from: data!)
                    DispatchQueue.main.async {
                        completed()
                    }
                }catch{
                    print("Error en JSON")
                }
            }
        }.resume()
    }
}
