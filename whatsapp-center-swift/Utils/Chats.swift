import Foundation
import UIKit

struct dataSend:Codable {
    let idcontacto: Int?
    let mensaje:String?
}
struct Chat:Decodable {
    let id:String?
    let body:String?
    let fromMe:Bool?
    //let _self:Int?
    //let author:String?
    let time:Int?
    //let messageNumber:Int?
    let type:String?
    //let chatName:String?
}
class Chats{
    var mensajes = [Chat]()
    var status:Bool = false
    
    func HttpGetMenesajes(idContacto:Int?,completed:@escaping () -> ()){
        let ruta = "https://whatsappcenter.tk:8443/api/mensaje/lista/\(idContacto!)"
        //print(ruta)
        cargarMensajes(ruta: ruta){
            completed()
        }
    }
        
    func cargarMensajes(ruta: String, completed: @escaping () -> ()){
        //var mensajesJSON:[Any]=[];
        let url = URL(string: ruta)
        URLSession.shared.dataTask(with: url!){ (data, response, error) in
            if error == nil{
                do{
                    self.mensajes = try JSONDecoder().decode([Chat].self, from: data!)
                    DispatchQueue.main.async {
                        completed()
                    }
                    /*
                    self.mensajes = mensajesJSON.map{(msg)-> Mensaje in
                        var men	saje = {
                            "id": msg!.id
                        }
                    }*/
                    //print("self.mensajes")
                    //print(self.mensajes)
                }catch{
                    print("Error en JSON")
                }
            }
        }.resume()
    }
    func HttpPostSendMenesajes(idContacto:Int?,completed:@escaping () -> ()){
        let ruta = "https://whatsappcenter.tk:8443/api/mensaje/enviar"
        //print(ruta)
        cargarMensajes(ruta: ruta){
            completed()
        }
    }
    
    
    func HttpPostSendMessages(data: dataSend,completed:@escaping () -> ()){
        let ruta = "https://whatsappcenter.tk:8443/api/mensaje/enviar"
        let url = URL(string: ruta )
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl )
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONEncoder().encode(data)
        let http = URLSession.shared.dataTask(with: request ) {( data, response,error) in
            if error != nil {
                print("Error en la peticion")
                return
            }
            self.status = true
            print("data")
            print(data)
            print("response")
            print(response)
            print("error")
            print(error)
        }
        http.resume()
    }
}
