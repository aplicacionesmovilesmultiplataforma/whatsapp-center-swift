//
//  Rol.swift
//  whatsapp-center-swift
//
//  Created by Mac 03 on 6/27/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import Foundation
struct  Rol:Decodable {
    let idrol:Int?
    let descripcion:String?
}
