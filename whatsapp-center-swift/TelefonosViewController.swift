import UIKit

class TelefonosViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var telefonos = [Telefono]()
    var anteriorVC:AsignarContactoViewController? = nil
    @IBOutlet weak var tablaTelefonos: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return telefonos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = telefonos[indexPath.row].numero
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        anteriorVC?.telefono = telefonos[indexPath.row]
        anteriorVC?.viewDidLoad()
        let util = Contactos()
        let idUsuario = Int(UserDefaults.standard.string(forKey: "UserIdUsuario")!)
        let idNumero = telefonos[indexPath.row].idnumero
        util.HttpGetContactos(idNumero: idNumero, idUsuario: idUsuario){
            self.anteriorVC?.contactos = util.contactos
            self.anteriorVC?.tableContactos.reloadData()
        }
        navigationController?.popViewController(animated: true)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tablaTelefonos.delegate = self
        self.tablaTelefonos.dataSource = self
        let util = Telefonos()
        let idEmpresa = Int(UserDefaults.standard.string(forKey: "UserIdEmpresa")!)
        util.HttpGetTelefonos(idEmpresa: idEmpresa){
            self.telefonos = util.telefonos
            self.tablaTelefonos.reloadData()
        }
    }
    
    

}
