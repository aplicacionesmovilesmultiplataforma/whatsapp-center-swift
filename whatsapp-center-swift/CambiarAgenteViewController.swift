import UIKit

class CambiarAgenteViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tablaAgentes: UITableView!
    var usuarios = [Usuario]()
    var anteriorVC:AsignarContactoViewController? = nil
    var contacto:Contacto? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tablaAgentes.delegate = self
        self.tablaAgentes.dataSource = self
        let idEmpresa = Int(UserDefaults.standard.string(forKey: "UserIdEmpresa")!)
        let util = Usuarios()
        util.HttpGetUsuarios(idEmpresa: idEmpresa){
            self.usuarios = util.usuarios
            self.tablaAgentes.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usuarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = usuarios[indexPath.row].nombres! + " - " + usuarios[indexPath.row].apellidos!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alerta = UIAlertController(title: "Cambiar Agente",message: "¿Esta seguro que desea asignar a \(self.usuarios[indexPath.row].nombres!) como agente de  \(self.contacto!.nombre!)?", preferredStyle: .alert)
        let btnOK = UIAlertAction(title: "Asignar", style: .default, handler: {(UIAlertAction) in
            let util = ActualizarAgente()
            let ruta = "https://whatsappcenter.tk:8443/api/contacto/actualizar"
            let datos = dataActualizarAgente(idusuario: self.usuarios[indexPath.row].idusuario, idcontacto: self.contacto?.idcontacto)
            util.HttpPutAgente(UrlHttp: ruta, data: datos){
                self.anteriorVC?.viewDidLoad()
                let util = Contactos()
                let idUsuario = Int(UserDefaults.standard.string(forKey: "UserIdUsuario")!)
                let idNumero = self.anteriorVC?.telefono?.idnumero
                util.HttpGetContactos(idNumero: idNumero, idUsuario: idUsuario){
                    self.anteriorVC?.contactos = util.contactos
                    self.anteriorVC?.tableContactos.reloadData()
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
        let btnCancel = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
        alerta.addAction(btnOK)
        alerta.addAction(btnCancel)
        self.present(alerta, animated: true, completion: nil)
    }

}
