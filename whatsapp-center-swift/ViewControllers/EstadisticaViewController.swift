//
//  EstadisticaViewController.swift
//  whatsapp-center-swift
//
//  Created by Mac 05 on 6/27/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import UIKit
import Charts

class EstadisticaViewController: UIViewController {
    
    
    
    @IBOutlet weak var chtChart: BarChartView!
    var reportes = [Reporte]()
    var colores = [UIColor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let idEmpresa = Int(UserDefaults.standard.string(forKey: "UserIdEmpresa")!)
        let idUsuario = Int(UserDefaults.standard.string(forKey: "UserIdUsuario")!)
        
        let utilC = Reportes()
        utilC.HttpGetReportes(idEmpresa: idEmpresa, idUsuario: idUsuario){
            self.reportes = utilC.reportes
            self.updateGraph()
            
        }
        
        configurar()
        
        // Do any additional setup after loading the view.
    }
    
    func configurar(){
        let l = chtChart.legend
        l.form = .circle
        l.formSize = 15
        l.font = UIFont(name: "HelveticaNeue-Light", size: 15)!
        l.xEntrySpace = 4
        
        colores.append(UIColor(red: 247/255, green:220/255, blue: 111/255, alpha: 1))
        colores.append(UIColor(red: 171/255, green:235/255, blue: 145/255, alpha: 1))
        colores.append(UIColor(red: 230/255, green:126/255, blue: 34/255, alpha: 1))
        colores.append(UIColor(red: 123/255, green:125/255, blue: 125/255, alpha: 1))
        colores.append(UIColor(red: 31/255, green:97/255, blue: 141/255, alpha: 1))
        colores.append(UIColor(red: 124/255, green:179/255, blue: 66/255, alpha: 1))
        colores.append(UIColor(red: 100/255, green:30/255, blue: 177/255, alpha: 1))
        colores.append(UIColor(red: 161/255, green:136/255, blue: 127/255, alpha: 1))
        colores.append(UIColor(red: 66/255, green:165/255, blue: 245/255, alpha: 1))
        colores.append(UIColor(red: 211/255, green:47/255, blue: 47/255, alpha: 1))
        colores.append(UIColor(red: 230/255, green:126/255, blue: 34/255, alpha: 1))
        
     
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateGraph(){
        var dataSets: [BarChartDataSet] = []
        
        var x = 0
        for reporte in self.reportes{
            var dataEntries: [BarChartDataEntry] = []
            let dataEntry = BarChartDataEntry(x: Double(x), y: reporte.contactos!)
            dataEntries.append(dataEntry)
           
            
            let chartDataSet = BarChartDataSet(entries: dataEntries, label: reporte.nombres)
            chartDataSet.colors = [self.colores[x]]
            
            dataSets.append(chartDataSet)
             x = x + 1
            
        }
        
        
        let chartData = BarChartData(dataSets: dataSets)
        chartData.barWidth = 0.6;
        
        
        
        
        chtChart.data = chartData
        
    }
    
}


