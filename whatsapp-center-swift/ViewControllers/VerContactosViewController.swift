//
//  VerContactosViewController.swift
//  whatsapp-center-swift
//
//  Created by Mac 17 on 6/21/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import UIKit
import SDWebImage

class ContactoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nombreContacto: UILabel!
    @IBOutlet weak var imagenContacto: UIImageView!
    @IBOutlet weak var fechaContacto: UILabel!
    @IBOutlet weak var telefonoContacto: UILabel!
}

class VerContactosViewController:UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
    
    @IBOutlet weak var tableContactos: UITableView!
    var telefono:Telefono? = nil
    var contactos = [Contacto]()
    var telefonos = [Telefono]()
    
    let activityIndicator = UIActivityIndicatorView()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableContactos.delegate = self
        self.tableContactos.dataSource = self
        activityIndicator.center = view.center
        activityIndicator.style = .large
        activityIndicator.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        activityIndicator.alpha = 0.5
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        let util = Telefonos()
        let idEmpresa = Int(UserDefaults.standard.string(forKey: "UserIdEmpresa")!)
         util.HttpGetTelefonos(idEmpresa: idEmpresa){
            print(util.telefonos)
            self.telefonos = util.telefonos
        }

        let utilC = Contactos()
        let idUsuario = Int(UserDefaults.standard.string(forKey: "UserIdUsuario")!)
        let idNumero = 1
        utilC.HttpGetContactos(idNumero: idNumero, idUsuario: idUsuario){
            self.contactos = utilC.contactos
            self.tableContactos.reloadData()
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }

        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactos.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "contactoCell", for: indexPath) as! ContactoTableViewCell
           // let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            cell.nombreContacto?.text = contactos[indexPath.row].nombre
        let numero = contactos[indexPath.row].codigo!
        cell.telefonoContacto?.text = "+ \(numero.prefix(9))"
            cell.fechaContacto?.text = contactos[indexPath.row].fconexion
            cell.imagenContacto?.sd_setImage(with: URL(string: contactos[indexPath.row].imagen!),completed: nil)
            //cell.imagenContacto?.layer.masksToBounds = true
            //cell.imagenContacto?.layer.cornerRadius =
            cell.imagenContacto?.layer.cornerRadius = (cell.imagenContacto?.frame.size.width ?? 0.0) / 2
            cell.imagenContacto?.clipsToBounds = true
            cell.imagenContacto?.layer.borderWidth = 3.0
            cell.imagenContacto?.layer.borderColor = UIColor.white.cgColor


            return cell
       }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueIniciarChat", sender: contactos[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "segueIniciarChat"){
            let siguienteVC = segue.destination as! ChatViewController
            siguienteVC.contacto = sender as? Contacto
        }
    }
    
    @IBAction func reloadContactsButton(_ sender: Any) {
        let util = Telefonos()
        let idEmpresa = Int(UserDefaults.standard.string(forKey: "UserIdEmpresa")!)
         util.HttpGetTelefonos(idEmpresa: idEmpresa){
            print(util.telefonos)
            self.telefonos = util.telefonos
        }
        let utilC = Contactos()
        let idUsuario = Int(UserDefaults.standard.string(forKey: "UserIdUsuario")!)
        let idNumero = 1
        utilC.HttpGetContactos(idNumero: idNumero, idUsuario: idUsuario){
            self.contactos = utilC.contactos
            self.tableContactos.reloadData()
        }
    }
    

}
