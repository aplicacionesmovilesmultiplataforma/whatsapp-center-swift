//
//  CrearUsuariosViewController.swift
//  whatsapp-center-swift
//
//  Created by Mac 17 on 6/21/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import UIKit
import iOSDropDown

class CrearUsuariosViewController: UIViewController {

    @IBOutlet weak var txtNombres: UITextField!
    @IBOutlet weak var txtApellidos: UITextField!
    @IBOutlet weak var dropRol: DropDown!
    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtClave: UITextField!
    var roles :  [Rol] = []
    var rolesId:[Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        let rol = UserDefaults.standard.string(forKey: "UserIdRol")!
        let ruta = "https://whatsappcenter.tk:8443/api/rol/lista/"+rol
        obtenerRoles(ruta: ruta){
            let rolesString:[String] = self.roles.map({(rol) in
                return rol.descripcion!
            })
            self.rolesId = self.roles.map({(rol) in
                return rol.idrol!
            })
            self.dropRol.optionIds = self.rolesId
            self.dropRol.optionArray = rolesString
        }
    }
    @IBAction func crearUsuario(_ sender: Any) {
        if validar(){
            let idempresa = UserDefaults.standard.string(forKey: "UserIdEmpresa")!
            let nombres = txtUsuario.text!
            let apellidos = txtApellidos.text!
            let idrol = self.rolesId[dropRol.selectedIndex!]
            let usuario = txtUsuario.text!
            let clave = txtClave.text!
            
            let datos = ["idempresa":"\(idempresa)","nombres":"\(nombres)","apellidos":"\(apellidos)","idrol":"\(idrol)","usuario":"\(usuario)","clave":"\(clave)"] as Dictionary<String,Any>
            let ruta = "https://whatsappcenter.tk:8443/api/usuario/registrar"
            
                            Metodo().metodo(ruta: ruta, datos: datos, method: "POST")
                            Alertas().alertaConAccion(self, title: "Usuario creado", message: "El usuario fue añadido correctamente", action: "OK",handler:{ alert in
                        self.navigationController?.popViewController(animated: true)
                            })
                           
        }
    }
    func validar()->Bool{
        return txtNombres.text!.count > 4 && txtApellidos.text!.count > 4 && dropRol.text!.count > 4 && txtUsuario.text!.count > 4 && txtClave.text!.count > 4
    }
    func obtenerRoles(ruta:String,completed: @escaping () -> ()) {
        let url = URL(string: ruta)
        URLSession.shared.dataTask(with: url!){
            (data,response,error) in
            if error == nil{
                do{
                    self.roles = try JSONDecoder().decode([Rol].self,from:data!)
                    DispatchQueue.main.async{
                        completed()
                    }
                }catch{
                    print("Error en JSON")
                }
            }
        }.resume()
    }
}
