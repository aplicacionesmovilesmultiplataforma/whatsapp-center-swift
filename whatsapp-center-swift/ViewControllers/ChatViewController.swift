//
//  ChatViewController.swift
//  whatsapp-center-swift
//
//  Created by Mac 14 on 6/24/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import UIKit
import SDWebImage
import MessageKit
import InputBarAccessoryView
import MessageInputBar
import Firebase
import AVFoundation
import FirebaseDatabase

struct Sender:SenderType{
    var senderId: String
    var displayName: String
    var photoURL:String
}

struct Message:MessageType{
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
    
}

struct Media: MediaItem {
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
}

class ChatViewController: MessagesViewController{
    
    var contacto:Contacto?
    
    var mensajes = [Message]()
    var mensajesChat = [Chat]()
    var senderContact:Sender?
    var senderMe:Sender?
    let activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(contacto)
        setupNavigationBarItems()
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messageInputBar.delegate = self
        
        
        activityIndicator.center = view.center
        activityIndicator.style = .large
        activityIndicator.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        activityIndicator.alpha = 0.5
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        navigationItem.largeTitleDisplayMode = .never
        maintainPositionOnKeyboardFrameChanged = true
        scrollsToLastItemOnKeyboardBeginsEditing = true
        
        senderContact = Sender(senderId: "1", displayName: contacto!.nombre!, photoURL: contacto!.imagen!)
        senderMe = Sender(senderId: "0", displayName: "Me", photoURL: "https://i.ibb.co/q0MwxdP/logo-green.png")
        let util = Chats()
        util.HttpGetMenesajes(idContacto: contacto!.idcontacto!){
            self.mensajesChat = util.mensajes
            //print("mensajesChat")
            //print(self.mensajesChat)
            var mediaImagen:Media?
            self.mensajes = self.mensajesChat.map{ (chat: Chat) -> Message in
                let senderH:Sender = chat.fromMe! ? self.senderMe! : self.senderContact!
                //print(chat.type)
                if chat.type! == "image"{
                    mediaImagen = Media(
                        url: URL(string: chat.body!),
                        image: nil,
                        placeholderImage: UIImage(systemName: "camera.fill")!,
                        size: .init(width: 250, height: 250)
                    )
                    return Message(
                            sender: senderH,
                            messageId: chat.id!,
                            sentDate: Date(timeIntervalSince1970: (TimeInterval(chat.time!))),
                            kind: .photo(mediaImagen!))
                }else{
                    return Message(
                            sender: senderH,
                            messageId: chat.id!,
                            sentDate: Date(timeIntervalSince1970: (TimeInterval(chat.time!))),
                            kind: .text(chat.body!))
                }

            }
            
            self.messagesCollectionView.reloadData()
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.messagesCollectionView.reloadDataAndKeepOffset()
            self.messagesCollectionView.scrollToLastItem()
            
        }
        setupInputButton()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        messageInputBar.inputTextView.becomeFirstResponder()
    }
    
    private func setupInputButton() {
        let cameraItem = InputBarButtonItem(type: .system)
        cameraItem.tintColor = UIColor(red: 30.0/255, green: 129.0/255, blue: 176.0/255, alpha: 1.0)
        cameraItem.image = UIImage(systemName: "camera.fill")
        cameraItem.addTarget(self, action: #selector(cameraPressed), for: .primaryActionTriggered)
        cameraItem.setSize(CGSize(width: 60, height: 30), animated: false)
        let audioItem = InputBarButtonItem(type: .system)
        audioItem.tintColor = UIColor(red: 30.0/255, green: 129.0/255, blue: 176.0/255, alpha: 1.0)
        audioItem.image = UIImage(systemName: "mic.fill")
        audioItem.addTarget(self, action: #selector(cameraPressed), for: .primaryActionTriggered)
        audioItem.setSize(CGSize(width: 60, height: 30), animated: false)
        messageInputBar.leftStackView.alignment = .center
        messageInputBar.setLeftStackViewWidthConstant(to: 100, animated: false)
        messageInputBar.setStackViewItems([cameraItem, audioItem], forStack: .left, animated: false)
    }
    
    private func setupNavigationBarItems(){
        let imagencontacto = UIImageView()
        imagencontacto.sd_setImage(with: URL(string: contacto!.imagen!), completed: nil)
        imagencontacto.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        imagencontacto.contentMode = .scaleToFill
        imagencontacto.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        imagencontacto.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
        imagencontacto.layer.cornerRadius = 25.0
        imagencontacto.clipsToBounds = true
        let titleLbl = UILabel()
        titleLbl.text = contacto!.nombre!
        titleLbl.textColor = UIColor.black
        titleLbl.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        let titleView = UIStackView(arrangedSubviews: [imagencontacto, titleLbl])
        titleView.spacing = 10.0
        navigationItem.titleView = titleView
        
    }
    @objc func cameraPressed() {
      presentInputActionSheet()
    }
    
    private func presentInputActionSheet(){
        
                let actionSheet = UIAlertController(title: "Elegir foto",
                                                    message: "¿De donde enviar foto?",
                                                    preferredStyle: .actionSheet)
                actionSheet.addAction(UIAlertAction(title: "Cámara", style: .default, handler: { [weak self] _ in
                    let picker = UIImagePickerController()
                    picker.sourceType = .camera
                    picker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    picker.allowsEditing = true
                    self?.present(picker, animated: true)
        
                }))
                actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { [weak self] _ in
        
                    let picker = UIImagePickerController()
                    picker.sourceType = .photoLibrary
                    picker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    picker.allowsEditing = true
                    self?.present(picker, animated: true)
        
                }))
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
                present(actionSheet, animated: true)
    }
    
    @IBAction func reloadChatButton(_ sender: Any) {
        let util = Chats()
        util.HttpGetMenesajes(idContacto: contacto!.idcontacto!){
            self.mensajesChat = util.mensajes
            //print("mensajesChat")
            //print(self.mensajesChat)
            self.mensajes = self.mensajesChat.map{ (chat: Chat) -> Message in
                let senderH:Sender = chat.fromMe! ? self.senderMe! : self.senderContact!
                return Message(
                        sender: senderH,
                        messageId: chat.id!,
                        sentDate: Date(timeIntervalSince1970: (TimeInterval(chat.time!))),
                        kind: .text(chat.body!))
            }
            
            self.messagesCollectionView.reloadData()
            self.messagesCollectionView.reloadDataAndKeepOffset()
            self.messagesCollectionView.scrollToLastItem()
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatViewController: InputBarAccessoryViewDelegate {
  func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
    let util = Chats()
    let data = dataSend(
        idcontacto: contacto!.idcontacto!,
        mensaje: text
    )
    let mensajeEnviar = Message(
        sender: senderMe!,
        messageId: UUID().uuidString,
        sentDate: Date(),
        kind: .text(data.mensaje!)
    )
    util.HttpPostSendMessages(data:data){
    }
    self.mensajes.append(mensajeEnviar)
    self.messageInputBar.inputTextView.text = ""
    /*
    util.HttpGetMenesajes(idContacto: self.idcontacto){
        self.mensajesChat = util.mensajes
        //print("mensajesChat")
        //print(self.mensajesChat)
        self.mensajes = self.mensajesChat.map{ (chat: Chat) -> Message in
            let senderH:Sender = chat.fromMe! ? self.senderMe! : self.senderContact!
            return Message(
                    sender: senderH,
                    messageId: chat.id!,
                    sentDate: Date(timeIntervalSince1970: (TimeInterval(chat.time!))),
                    kind: .text(chat.body!))
        }
        //print("mensajes")
        //print(self.mensajes)
        self.messagesCollectionView.reloadData()
        DispatchQueue.main.async {
            self.messagesCollectionView.reloadDataAndKeepOffset()
            self.messagesCollectionView.reloadData()
         self.messagesCollectionView.scrollToItem(at: IndexPath(row: 0, section: self.mensajes.count - 1), at: .top, animated: false)
        }
    }*/
    print(self.mensajes)
    
    self.messageInputBar.inputTextView.resignFirstResponder()
    self.messagesCollectionView.reloadData()
    self.messagesCollectionView.reloadDataAndKeepOffset()
    self.messagesCollectionView.scrollToLastItem()
    }
    
}

extension ChatViewController:MessagesDataSource, MessagesLayoutDelegate, MessagesDisplayDelegate {
    func currentSender() -> SenderType {
        senderMe!
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return mensajes[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return mensajes.count
    }
    func configureMediaMessageImageView(_ imageView: UIImageView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        guard let message = message as? Message else {
            return
        }

        switch message.kind {
        case .photo(let media):
            guard let imageUrl = media.url else {
                return
            }
            imageView.sd_setImage(with: imageUrl, completed: nil)
        default:
            break
        }
    }
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let imagenesFolder = Storage.storage().reference().child("imagenes")
           if let image = info[.editedImage] as? UIImage, let imageData =  image.pngData() {
           var imagenID = NSUUID().uuidString
           let cargarImagen = imagenesFolder.child("\(imagenID).jpg")
            cargarImagen.putData(imageData, metadata: nil) { (metadata, error) in
              if error != nil{
                 print("Ocurrio un error al subir imagen \(error)")
              }else{
                 cargarImagen.downloadURL(completion: {(url, error) in
                    guard let enlaceURL = url else{
                          print("Ocurrio un error al obtener informacion de la imagen \(error)")
                          return
                    }
                    guard let placeholder = UIImage(systemName: "camera.fill") else {
                            return
                    }
                    let media = Media(url: enlaceURL,
                                      image: nil,
                                      placeholderImage: placeholder,
                                      size: .init(width: 250, height: 250))
                    let message = Message(sender: self.senderMe!,
                                         messageId: imagenID,
                                         sentDate: Date(),
                                         kind: .photo(media))
                    let msgMedia = ["kind": "photo", "url": enlaceURL.absoluteString]
                    Database.database().reference().child("\((self.contacto!.idcontacto)!)").child("imagenes").child("me").child(imagenID).setValue(msgMedia)
                    let util = Chats()
                    let data = dataSend(
                        idcontacto: self.contacto!.idcontacto,
                        mensaje: "p|\(imagenID)|\(enlaceURL)"
                     )
                     
                  util.HttpPostSendMessages(data:data){
                  }
                  self.mensajes.append(message)
                  self.messageInputBar.inputTextView.text = ""
                  print(self.mensajes)
                  
                  self.messageInputBar.inputTextView.resignFirstResponder()
                  self.messagesCollectionView.reloadData()
                  self.messagesCollectionView.reloadDataAndKeepOffset()
                  self.messagesCollectionView.scrollToLastItem()
        
                    
        
                 })
              }
           }
        }
    }
    
}
