//
//  CambiarClaveViewController.swift
//  whatsapp-center-swift
//
//  Created by Mac 17 on 6/21/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import UIKit
import FirebaseAuth

class CambiarClaveViewController: UIViewController {

    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmNewPassword: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
      
    @IBAction func cambiarPasswordTapped(_ sender: Any) {
        Auth.auth().signIn(withEmail: UserDefaults.standard.string(forKey: "UserUsuario")!, password: txtOldPassword.text!)
        {(user,err) in
            if err != nil{//Caso de contraseña anterior incorrecta
                Alertas().alertaSimple(self,title:"Credenciales Incorrectas",message:"Contraseña Anterior Incorrecta",action:"OK")
                
            }//Si la contraseña anterior es correcta se evalua que las constraseñas introducidas sean iguales
            else if self.txtNewPassword.text! == self.txtConfirmNewPassword.text!{
                let idusuario =
UserDefaults.standard.string(forKey: "UserIdUsuario")!
                let idempresa = UserDefaults.standard.string(forKey: "UserIdEmpresa")!
                let nombres = UserDefaults.standard.string(forKey: "UserNombre")!
                let apellidos = UserDefaults.standard.string(forKey: "UserApellido")!
                let idrol = UserDefaults.standard.string(forKey: "UserIdRol")!
                let usuario = UserDefaults.standard.string(forKey: "UserUsuario")!
                let clave = self.txtNewPassword.text!
                
                let datos = ["idusuario":"\(idusuario)","idempresa":"\(idempresa)","nombres":"\(nombres)","apellidos":"\(apellidos)","idrol":"\(idrol)","usuario":"\(usuario)","clave":"\(clave)"] as Dictionary<String,Any>
                let ruta = "https://whatsappcenter.tk:8443/api/usuario/actualizar"
                
                Metodo().metodo(ruta: ruta, datos: datos, method: "PUT")
                Alertas().alertaConAccion(self, title: "Contraseña editada", message: "Su contraseña a sido editada correctamente", action: "OK",handler:{ alert in
            self.navigationController?.popViewController(animated: true)
                })
               
                
            }else{//Contraseñas no coinciden
                Alertas().alertaSimple(self,title:"Contraseñas No Coinciden",message:"Las Contraseñas Proporcionadas no son iguales",action:"OK")
                
            }
        }
    }
    
}
