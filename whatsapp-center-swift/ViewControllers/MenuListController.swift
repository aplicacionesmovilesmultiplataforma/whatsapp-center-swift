//
//  MenuListController.swift
//  whatsapp-center-swift
//
//  Created by Mac 17 on 6/21/21.
//  Copyright © 2021 Whatsapp-Center. All rights reserved.
//

import UIKit

class MenuListController: UITableViewController {
    
    let rol = UserDefaults.standard.string(forKey: "UserIdRol")
    var listMenu = ["Inicio", "Ver Contactos", "Cambiar Contraseña", "Estadistica"]
    var homeViewController: HomeViewController = HomeViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if rol == "1" {
            listMenu.append(contentsOf: ["Asignar Contacto", "Crear Usuarios"])
        }

        let gradientBackgroundColors = [UIColor.link, UIColor.white]
        let gradientLocations = [0.0,1.0]
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientBackgroundColors
        gradientLayer.locations = gradientLocations as [NSNumber]
        gradientLayer.frame = tableView.bounds
        let backgroundView = UIView(frame: tableView.bounds)
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        tableView.backgroundColor = UIColor.link
        
        tableView.alwaysBounceVertical = false
        tableView.separatorStyle = .none
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    init(_ home: HomeViewController) {
        homeViewController = home
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listMenu.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = listMenu[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
        cell.imageView?.tintColor = UIColor.white
        
        
        switch indexPath.row {

        case 1:
            cell.imageView?.image = UIImage(systemName: "person.2.fill")
        case 2:
            cell.imageView?.image = UIImage(systemName: "wrench.fill")
        case 3:
            cell.imageView?.image = UIImage(systemName: "chart.bar.fill")
        case 4:
            cell.imageView?.image = UIImage(systemName: "person.crop.circle.badge.exclam.fill")
        case 5:
            cell.imageView?.image = UIImage(systemName: "person.badge.plus.fill")
            
        default:
            cell.imageView?.image = UIImage(systemName: "house.fill")
        }
        // 
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option = listMenu[indexPath.row]

        switch option {
            
        case "Ver Contactos":
            dismiss(animated: true) {
                self.homeViewController.performSegue(withIdentifier: "segueShowContacts", sender: nil)
            }
        case "Cambiar Contraseña":
            dismiss(animated: true) {
                self.homeViewController.performSegue(withIdentifier: "segueChangePassword", sender: nil)
            }
        case "Asignar Contacto":
            dismiss(animated: true) {
                self.homeViewController.performSegue(withIdentifier: "segueAssignContact", sender: nil)
            }
        case "Crear Usuarios":
            dismiss(animated: true) {
                self.homeViewController.performSegue(withIdentifier: "segueCreateUsers", sender: nil)
            }
        case "Estadistica":
           dismiss(animated: true) {
               self.homeViewController.performSegue(withIdentifier: "segueEstadistica", sender: nil)
           }
            
        default:
            dismiss(animated: true, completion: nil)
        }
    }
}
