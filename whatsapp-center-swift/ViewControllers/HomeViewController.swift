import SideMenu
import UIKit

class HomeViewController: UIViewController {

    var menu: SideMenuNavigationController?
    let rql = Login()
    
    @IBOutlet weak var logoHomeImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.animate(withDuration: 1, animations: {
            self.logoHomeImageView.frame.origin.y -= 200
        })
        
        menu = SideMenuNavigationController(rootViewController: MenuListController(self))
        menu?.leftSide = true
        menu?.menuWidth = 300
        menu?.presentDuration = 0.8
        menu?.setNavigationBarHidden(true, animated: false)
        
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)

    }
    
    @IBAction func showMenu(_ sender: Any) {
        present(menu!, animated: true)
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        showAlert("Cerrar Sesión", "¿Estás seguro de salir de aplicación?")
    }
    
    func showAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { (action) in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { (action) in
            self.rql.RemoveUserPreferences()
            self.dismiss(animated: true, completion: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
}

