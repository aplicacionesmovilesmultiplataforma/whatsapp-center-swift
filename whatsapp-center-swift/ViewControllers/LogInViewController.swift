import UIKit
import Firebase
import FirebaseAuth

class LogInViewController: UIViewController {

    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Comprobar si el usuario ya esta autentificado
        if UserDefaults.standard.string(forKey: "UserIdEmpresa") != nil
            {
            performSegue(withIdentifier: "segueUsuarioActivo", sender: nil)
                print("Bienvenido de nuevo :)")
        }
    }
    @IBAction func btnLogin(_ sender: Any) {
        Auth.auth().signIn(withEmail: txtUser.text!, password: txtPassword.text!){ (user,err) in
            if err != nil {
                let alertaError = UIAlertController(title: "Credenciales incorrectas", message: "Correo o contraseña incorrecta", preferredStyle: .alert)
                let btnAceptar = UIAlertAction(title: "Aceptar", style: .default)
                alertaError.addAction(btnAceptar)
                self.present(alertaError, animated: true, completion: nil)
            }else{
                
                let rql = Login()
                let data = dataUser(usuario: self.txtUser.text!, clave: self.txtPassword.text!)
                let uri:String = "https://whatsappcenter.tk:8443/api/usuario/login"
                rql.HttpPostLogin(UrlHttp: uri, data: data){
                    if rql.usuarios[0].usuario == self.txtUser.text!{
                        rql.UserPreferences(idusuario: rql.usuarios[0].idusuario!,
                                            idempresa: rql.usuarios[0].idempresa!,
                                            nombres: rql.usuarios[0].nombres!,
                                            apellidos: rql.usuarios[0].apellidos!,
                                            idrol: rql.usuarios[0].idrol!,
                                            rol: rql.usuarios[0].rol!,
                                            usuario: rql.usuarios[0].usuario!)
                        self.performSegue(withIdentifier: "segueLogin", sender: nil)
                    }else{
                        print("Error en login")
                    }
                }
            }
        }
    }
}

